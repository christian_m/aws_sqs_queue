resource "aws_sqs_queue" "default" {
  name                        = var.queue_name
  fifo_queue                  = var.fifo_enabled
  content_based_deduplication = var.fifo_enabled
  redrive_policy              = jsonencode({
    "deadLetterTargetArn" : aws_sqs_queue.dead_letter.arn,
    "maxReceiveCount" : 5
  })

  visibility_timeout_seconds = 300

  tags = {
    env = var.environment
  }
}

resource "aws_sqs_queue" "dead_letter" {
  name                        = "${var.queue_name}_dl"
  fifo_queue                  = var.fifo_enabled
  content_based_deduplication = var.fifo_enabled

  tags = {
    env = var.environment
  }
}

resource "aws_sqs_queue_policy" "default" {
  queue_url = aws_sqs_queue.default.id

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Id" : "${var.queue_name}-policy",
    "Statement" : [
      {
        "Sid" : "First",
        "Effect" : "Allow",
        "Principal" : "*",
        "Action" : "sqs:SendMessage",
        "Resource" : aws_sqs_queue.default.arn,
        "Condition" : {
          "ArnEquals" : {
            "aws:SourceArn" : var.queue_source_arn
          }
        }
      }
    ]
  })
}
