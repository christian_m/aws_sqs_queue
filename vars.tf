variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "queue_name" {
  description = "name of the queue"
  type        = string
}

variable "queue_source_arn" {
  description = "source arn of the queue"
  type        = string
}

variable "fifo_enabled" {
  description = "enable fifo queue"
  type        = bool
  default     = false
}