output "queue_arn" {
  value = aws_sqs_queue.default.arn
}

output "queue_name" {
  value = aws_sqs_queue.default.name
}

output "dl_queue_name" {
  value = aws_sqs_queue.dead_letter.name
}